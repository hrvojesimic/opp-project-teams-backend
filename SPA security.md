# Promjene na BE koje omogućuju lakši rad sa SPA

Kako bi naša SPA mogla raditi na predviđen način potrebno je prvo napraviti neke izmjene pri postavljanju `SecurityFilterChain`.
Glavna razlika između postojećeg rješenja i rješenja koja su pogodne za SPA su tipovi odgovora koje naš server šalje. SPA ne
očekuju redirect nego očekuju odgovarajući HTTP status i potencijalno neke dodatne podatke kako bi na temelju toga zaključile
kako promijeniti UI.

Za pokretanje serverske aplikacije koristeći SPA `SecurityFilterChain` potrebno je u `application.properties` postavku
profila `spring.profiles.active` promijeniti u `basic-security`

## Konfiguracija form logina

```java
http.formLogin(configurer -> {
        configurer.successHandler((request, response, authentication) ->
                response.setStatus(HttpStatus.NO_CONTENT.value())
            )
            .failureHandler(new SimpleUrlAuthenticationFailureHandler());
    }
);

http.exceptionHandling(configurer -> {
final RequestMatcher matcher = new NegatedRequestMatcher(
    new MediaTypeRequestMatcher(MediaType.TEXT_HTML));
    configurer
    .defaultAuthenticationEntryPointFor((request, response, authException) -> {
    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }, matcher);
    });
```

Pozivom `successHandler` metode definirano da na uspješni login ne želimo redirect nego HTTP 204, dok pozivom
`failureHandler` metode s parametrom `new SimpleUrlAuthenticationFailureHandler()` osiguravamo da se pošalje 403 ili
napravi redirect. 

Kako bi skroz uklonili redirect prilikom zvanja iz aplikacije iz SPA tj pozivima koji za `Content-Type` nemaju postavljen
`text/html` (defaultni Content-Type za GET u browseru) potrebno je konfigurirati `exceptionHandling`. Postavkama u 
`exceptionHandling` definiramo da se na svaki poziv našeg servera s gore navedenim kriterijma vrati HTTP 403 tj
`response.setStatus(HttpServletResponse.SC_UNAUTHORIZED)`

## Konfiguracija logout-a 

```java
http.logout(configurer -> configurer
    .logoutUrl("/logout")
    .logoutSuccessHandler((request, response, authentication) ->
        response.setStatus(HttpStatus.NO_CONTENT.value())));
```
Konfiguracija je slična onoj za `formLogin`, `logoutUrl` metodom definira se url na kojem želimo da se odrađuje logout.
`.logoutSuccessHandler((request, response, authentication) -> response.setStatus(HttpStatus.NO_CONTENT.value())))` definira
response na uspješni logut.

## Konfiguracija ruta

Budući da smo dodali novu rutu koja je ulaz u app, tu rutu je potrebno ostaviti ne osiguranu, a to ćemo napraviti pomoću
`requestMatchers().permitAll()`
```java
http.authorizeHttpRequests(authorize -> authorize
    .requestMatchers(new AntPathRequestMatcher("/login")).permitAll()
    .anyRequest().authenticated());
```
