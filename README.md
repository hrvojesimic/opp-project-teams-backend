# Spring Boot backend demo aplikacija

Demo aplikacija Spring Boot backenda za potrebe kolegija Programsko inženjerstvo, provided by [CROZ](http://croz.net).

Slajdovi s predavanja 21.10.2024. u PDF-u:
[1. dio](https://drive.google.com/file/d/1-wDPs1UNlc8u1rH91Su-9MAvZ7z72wGf/view?usp=sharing),
[2. dio](https://drive.google.com/file/d/106HIUuC4FH5FfiNrajK9sCyN5iyzcybc/view?usp=sharing)

## Autentikacija

Pretpostavljeni (_default_) Spring profil se zove `basic-security` i koristi _HTTP Basic Authentication_, svi korisnici imaju
zaporku `pass`.

Ako se Spring Boot aplikacija pokreće u funkciji backenda za React frontend kako je prikazana u drugom tehničkom predavanju,
potrebno ju je pokrenuti s Spring profilom `oauth-security` i postaviti potrebnu konfiguraciju u `application.properties`:

- `spring.security.oauth2.client.registration.google.client-id`
- `spring.security.oauth2.client.registration.google.client-secret`

Client ID i secret ćete dobiti kad otvorite svojeg OAuth klijenta kroz Google development konzolu.

Na drugom predavanju je prikazan i treći način autentikacije putem Web obrasca, za kojeg je potrebno postaviti
Spring profil `form-security`.

## Funkcionalnosti dodane na one s predavanja

U odnosu na verziju s predavanja, konačan kôd backenda na [`master` grani](https://gitlab.com/hrvojesimic/progi-project-teams-backend/-/tree/master) ima još:

 - dohvat studenta i grupe po ID-u
 - ažuriranje studenta metodom PUT
 - brisanje studenta metodom DELETE
 - izmjena imena grupe metodom PATCH
 - dodatne provjere pri kreiranju grupe
 - dodavanje i micanje članova iz grupe posebnim korištenjem metoda PUT i DELETE
 - ograničenje na veličinu grupe
 - inicijalizacija baze s nekoliko testnih studenata
 - primjeri testova

Probleme s projektom i sva pitanja možete unijeti kao [issue](https://gitlab.com/hrvojesimic/progi-project-teams-backend/issues).
