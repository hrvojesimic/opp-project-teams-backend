# Izrada REST backenda u Spring Bootu

U ovom dokumentu imate niz predavanja i vježbi korak-po-korak koje će vam pomoći da bolje razumijete tehnologiju backenda koji izlaže poslovnu funkcionalnost putem REST Web servisa, a podatke čuva u relacijskoj bazi.

**Pažnja: video materijali su snimani u doba pandemije i postoje manje razlike u odnosu na najnovije verzije alata i programskih biblioteka. No ništa nije koncepcijski drugačije.**

## Osnove

Ova video predavanja vam daju kratak pregled nekih tema koje su preduvjet za izradu našeg backenda. Ako nešto ne znate od ranije, pogledajte sljedeća video predavanja:

- [Apache Maven](https://drive.google.com/open?id=1ET6iTK_tBjbMkwcadlUHOlthuickd5bP)
- [REST stil razvoja Web servisa](https://drive.google.com/open?id=1Ltob4s1DBCKOyuvVno5Uf1LdteGQVw7t)
- [JSON format](https://drive.google.com/open?id=1kw4O8IHn_QESON5yug_1UwLkVvq1h9rR)
- [Dizajn REST servisa](https://drive.google.com/open?id=1Ba27sScJu7aIktkuRhqSM1RD6vfrHAzx )

## Spring Framework

Vjerojatno će vam najveći zalogaj u učenju tehnologija predstavljati Spring Framework i srodni Spring projekti.

Spring je široki skup biblioteka i alata koji olakšava razvoj aplikacija u Javi. Za razliku od "Enterprise Jave", ne zahtjeva aplikacijske poslužitelje. Također, minimalno se petlja u vaš kod. Rijetko kad zahtjeva da implementirate interface okvira, još rjeđe da izvedete potklasu iz njegove. No često traži označavanje koda kroz anotacije. Isto tako, preferira notacije i konvencije, koristeći oblikovne obrasce poput _template_-a i aspektno-orijentiranog programiranja.

Jezgra Springa je tzv. _inversion of control container_ ili **aplikacijski kontekst**, koji se temelji na oblikovnom obrascu koji se zove _dependency injection_. O korištenju tog obrasca u vašem kodu ovise skoro svi drugi mehanizmi kroz koje Spring pomaže u razvoju vaše aplikacije. Kako se to provodi u Springu pojašnjava ovaj dokument:

[Dependency injection u Springu - praktične vježbe](https://drive.google.com/file/d/1NNMDrBAjLSF6NDpkUPtKWcgXhY-D0hH4/view?usp=sharing) 

Dokument pokazuje kako izgleda dependency injection na više načina, od "starog" putem XML-a, sve do implicitno definiranog automatskog ožičavanja koje koristi Spring Boot. Iako u svom projektu ne trebate koristiti XML konfiguraciju, koristi razumijeti kako ožičavanje radi eksplicitno. Kasnije ćete se lakše izvući iz zapetljanih situacija koje će se pojaviti s automatskim ožičavanjem ako znate što se događa "ispod haube".

## Spring Boot

Da bi u naš program lakše uključili i integrirali sve module koje će nam trebati za izradu Web aplikacije, mapiranje objektnog modela na relacijsku bazu podataka, postavili sigurnosne provjere i slično, od velike koristi će nam doći Spring Boot. U njemu su kodificirane najbolje prakse u arhitekturi Spring aplikacija, po načelu _convention over configuration_: "radije se držite konvencija nego da morate posebno konfigurirati".

Spring Boot vam radi automatsko podešavanje i povezivanje različitih modula, tako da analizira što ste uključili u _classpath_ i sam zaključuje kako te module najbolje integrirati u smislenu cjelinu. Također dodaje korisne razvojne alate i podršku produkciji.

Ovaj (stariji) video objašnjava kako kreirati novi Spring Boot projekt: [Korištenje start.spring.io](https://drive.google.com/file/d/1RcOyZr6LbYmPgm760LFZJr_sjaDlWvaD/view)

Za novi razvoj trebat ćete koristiti ažurnije verzije Springa i Jave. Preporučam:

- zadnju stabilnu verziju Spring Boota
- Javu 17

ZIP datoteku koju dobijete trebate raspakirati, i direktorij otvoriti u IntelliJ-u. Pogledajte ovaj video u kojem se pokazuje kako pokrenuti Spring Boot aplikaciju: [Pokretanje prve Spring Boot aplikacije](https://drive.google.com/file/d/1HX1SDvBw8S70SOOlNwO2xl8m56Vv7G2x/view)

Nakon što ste kreirali Spring Boot projekt, pogledajte i isprobajte kako se pišu Web servisi u REST stilu:

- [Predavanje: REST servisi u Springu](https://drive.google.com/open?id=1sCmS2VEpq1zaQq29uqHVtSh0-dTDPukR)
- [Vježba: Izrada prvih kontrolera u Springu](https://drive.google.com/file/d/1608KLBhlur1RGv5NgVjOKWpvm3V5uvwG/view)

## Tutorial izrade malog backenda u Spring Bootu

Tutorial s tehničkog predavanja na kolegiju koji vas vodi kroz izradu backenda za organizaciju studenata u projektne grupe, korak po korak. **Pažnja: ovaj verzija materijala je iz 2020. i postoje manje razlike u odnosu na predavanje 2022.**

1. [Nova aplikacija](https://drive.google.com/open?id=102h7Q6u14xn-h09V3BKu_6zDoOim19u7)
2. [Domenski model i ORM](https://drive.google.com/open?id=1BN76EshFbTv_2xA7RwASxmwihn2PVO2J)
3. [Troslojna arhitektura](https://drive.google.com/open?id=1FXakR0y2_pFiQg0ey_CKCpmblSpzBR2Q)
4. [Umetanje novog entiteta](https://drive.google.com/open?id=1RXyUZc036tC3Y2UuGO7gWkzuwsYBmtmQ)

### Upiti nad bazom

5. [Naši upiti](https://drive.google.com/open?id=1NEHr_0NSEoAczfWE9bmT3N3DTXW2WlSu)
6. [Exception handling](https://drive.google.com/open?id=1sfQi_CAIqPFZtudIuA3_wdN2EKLttQwO)
7. [Druga domenska klasa](https://drive.google.com/open?id=1XH4R7yUVE7nBQaFrIlRyjxg0VXJKUb9Y)
8. [Kreiranje nove grupe](https://drive.google.com/open?id=1NnyN-7kRpuWVEFhTY923W5x4i7lW7Yys)
9. [JPQL](https://drive.google.com/open?id=1Ml2OPXYzjC8xLgNUeiTwqoffCUIFY3bh)

### Security

10. [Postavljanje securityja](https://drive.google.com/open?id=17LyaIGkcnRZemHmyVRRPU9SkKVKPtldW)
11. [Postavljanje korisnika](https://drive.google.com/open?id=1BDavH4aSdpW80RKdAYANuV-Q6amtAcwc)
12. [Autorizacija zahtjeva](https://drive.google.com/open?id=1ZML4dMAQJxe1tjSD7w9K6RNkXwJK38cu)
13. [Korisnici u bazi](https://drive.google.com/open?id=1UhaE6P3e_75yqCuk35IfSLgdsbr5wjCn)
14. [Prepoznavanje ulogiranog korisnika](https://drive.google.com/open?id=14CS7UP5HBXwYuoDJnz-Sqgfhv6Y9DGEb)
