const BASE_URI = 'http://localhost:8080/';
const studentList = document.getElementById('student-list');

loadStudents();

function loadStudents() {
  fetch(BASE_URI + 'students')
    .then( response => response.json() )
    .then( students => {
      studentList.innerHTML = students.map(studentHtml).join("")
    });
}

function studentHtml(s, removed) {
  return `
    <li id="student-${s.id}" ${s.removed? 'class="removed"' : ''}>
      <span class="id">${s.id}</span>
      <span class="given name">${s.givenName}</span>
      <span class="family name">${s.familyName}</span>
      ${s.lead? '<span class="lead">★</span>' : ''}
      <span class="jmbag">${s.jmbag}</span>
      <span onclick="deleteStudent(${s.id})"
            class="delete">✕</span>
    </li>
  `;
}

function deleteStudent(id) {
  const request = { method: 'DELETE' };
  fetch(BASE_URI + 'students/' + id, request)
    .then(response => {
      if (response.ok) {
        document.getElementById('student-' + id).classList.add('removed');
      }
      else {
        response.json().then( o => alert(o.message) );
      }
    });
}

function inputEl(name) {
  return document.querySelector(
    `#addStudentForm input[name='${name}']`
  );
}

function studentFromInput() {
  return {
    givenName:  inputEl('givenName').value,
    familyName: inputEl('familyName').value,
    lead:       inputEl('lead').checked,
    jmbag:      inputEl('jmbag').value
  };
}

function addNewStudent() {
  const request = {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(studentFromInput())
  };
  fetch(BASE_URI + 'students', request)
    .then(handleNewStudentResponse);
}

function handleNewStudentResponse(response) {
  if (response.ok) {
    document.getElementById('addStudentForm').reset();
    const newStudentUri = response.headers.get('Location');
    fetch(newStudentUri)
      .then( r => r.json() )
      .then( s => studentList.appendChild(studentNode(s)) )
      .then( f => f.classList.remove('removed') );
  }
  else {
    response.json().then( body => alert(body.message) );
  }
}

function studentNode(s) {
  const f = document.createElement('template');
  f.innerHTML = studentHtml(s, true).trim();
  return f.content;
}