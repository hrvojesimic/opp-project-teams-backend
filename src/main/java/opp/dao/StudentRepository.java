package opp.dao;

import opp.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StudentRepository extends JpaRepository<Student, Long> {

  Optional<Student> findByJmbag(String jmbag);

  int countByJmbag(String jmbag);

  // Note: exists- query is the best if we just want to predict conflicts
  boolean existsByJmbagAndIdNot(String jmbag, Long id);
}
