package opp.dao;

import opp.domain.Group;
import opp.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface GroupRepository extends JpaRepository<Group, Long> {

  Optional<Group> findByName(String groupName);

  @Query("SELECT g FROM SGROUP g WHERE :s MEMBER OF g.members")
  Optional<Group> findByMember(@Param("s") Student student);

  boolean existsByNameAndIdNot(String name, Long id);
}
