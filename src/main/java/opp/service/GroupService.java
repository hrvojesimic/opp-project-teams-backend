package opp.service;

import opp.domain.Group;
import opp.domain.Student;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Manages groups in the system.
 * @see Group
 * @author Hrvoje Šimić hsimic@croz.net
 */
@Service
public interface GroupService {

  /**
   * Lists all groups.
   * @return iterable containing all groups
   */
  List<Group> listAll();

  /**
   * Finds group with given ID, if exists.
   * @param groupId given group ID
   * @return Optional with value of group associated with given ID in the system,
   * or no value if one does not exist
   * @see GroupService#fetch
   */
  Optional<Group> findById(long groupId);

  /**
   * Fetches group with given ID.
   * @param groupId given group ID
   * @return group associated with given ID
   * @throws EntityMissingException if group with that ID not found in the system
   */
  Group fetch(long groupId);

  /**
   * Creates new group with given coordinator and name.
   * Coordinator cannot be changed later, and is always member of this group.
   * @param groupName name of the new group
   * @param coordinatorJmbag JMBAG of student assigned as coordinator of the new group
   * @return created Group object, with ID set and member list consisting only of group coordinator
   * @throws IllegalArgumentException if name is empty or any is <code>null</code>
   * @throws RequestDeniedException if no student with given JMBAG,
   * or student with that JMBAG is already a member of another group
   */
  Group createGroup(String groupName, String coordinatorJmbag);

  /**
   * Updates the name of a given group.
   * @param groupId identifies group to update
   * @param name new name of the group
   * @return updated group object
   * @throws EntityMissingException if entity with the same ID as in parameter does not exist
   * @throws IllegalArgumentException if name is empty or any is <code>null</code>
   */
  Group updateGroupName(long groupId, String name);

  /**
   * Lists all members of group, including the coordinator.
   * @param groupId ID of group to list
   * @return set contains all members of the group
   * @throws EntityMissingException if group not found
   * @throws IllegalArgumentException if argument <code>null</code>
   */
  Set<Student> listMembers(long groupId);

  /**
   * Adds a member to given group. Member cannot be added if in another group.
   * @param groupId identifies group to which given member is to be added
   * @param studentId identifies student to add
   * @return <code>true</code> if member was newly added to group,
   * <code>false</code> if it was already a member.
   * @throws EntityMissingException if an entity with one or the other ID is not found
   * @throws RequestDeniedException if already member of another group, or group too large
   * @throws IllegalArgumentException if any argument <code>null</code>,
   * or student is lead
   */
  boolean addMember(long groupId, long studentId);

  /**
   * Removes given member from given group. Group's coordinator cannot be removed.
   * @param groupId identifies group from which to remove given member
   * @param studentId identifies student to remove
   * @return <code>true</code> if member was removed from group,
   * <code>false</code> if the student wasn't a member to begin with.
   * @throws EntityMissingException if an entity with one or the other ID is not found
   * @throws RequestDeniedException if coordinator of given group
   * @throws IllegalArgumentException if any argument <code>null</code>
   */
  boolean removeMember(long groupId, long studentId);
}
