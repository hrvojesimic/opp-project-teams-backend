package opp.service.impl;

import opp.dao.GroupRepository;
import opp.domain.Group;
import opp.domain.Student;
import opp.service.EntityMissingException;
import opp.service.GroupService;
import opp.service.RequestDeniedException;
import opp.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class GroupServiceJpa implements GroupService {
  @Autowired
  private StudentService studentService;

  @Autowired
  private GroupRepository groupRepo;

  @Value("${opp.group.max-size}")
  private int groupMaxSize;

  @Override
  public List<Group> listAll() {
    return groupRepo.findAll();
  }

  @Override
  public Optional<Group> findById(long groupId) {
    return groupRepo.findById(groupId);
  }

  @Override
  public Group fetch(long groupId) {
    return findById(groupId).orElseThrow(
      () -> new EntityMissingException(Group.class, groupId)
    );
  }

  @Override
  public Group createGroup(String groupName, String coordinatorJmbag) {
    Student coordinator = studentService.findByJmbag(coordinatorJmbag).orElseThrow(
      // NOTE: not throwing EntityMissingException because that is just for missing resources from URI
      () -> new RequestDeniedException("No student with JMBAG " + coordinatorJmbag)
    );
    Assert.isTrue(coordinator.isLead(),
      "Group coordinator must be a lead, not: " + coordinator);
    groupRepo.findByMember(coordinator).ifPresent(g -> {
      throw new RequestDeniedException(coordinator + " already member of " + g);
    });
    groupRepo.findByName(groupName).ifPresent(g -> {
      throw new RequestDeniedException(groupName + " already name of " + g); }
    );
    return groupRepo.save(new Group(groupName, coordinator));
  }

  @Override
  public Group updateGroupName(long groupId, String name) {
    Assert.hasText(name, "Group name must be provided");
    Group group = fetch(groupId);
    if (groupRepo.existsByNameAndIdNot(name, groupId)) {
      throw new RequestDeniedException("Another group already has name '" + name + "'");
    }
    group.setName(name);
    return groupRepo.save(group);
  }

  @Override
  public Set<Student> listMembers(long groupId) {
    return fetch(groupId).getMembers();
  }

  @Override
  public boolean addMember(long groupId, long studentId) {
    Group group = fetch(groupId);
    Student student = studentService.fetch(studentId);
    Assert.isTrue(!student.isLead(), "Must be non-lead to be a member: " + student);
    groupRepo.findByMember(student).filter(g -> !g.getId().equals(groupId)).ifPresent(g -> {
      throw new RequestDeniedException(student + " already member of " + g); }
    );
    if (group.getMembers().size() >= groupMaxSize)
      throw new RequestDeniedException("Already at max size (" + groupMaxSize + "): " + group);
    boolean added = group.getMembers().add(student);
    if (added)
      groupRepo.save(group);
    return added;
  }

  @Override
  public boolean removeMember(long groupId, long studentId) {
    Group group = fetch(groupId);
    if (group.getCoordinator().getId().equals(studentId))
      throw new RequestDeniedException("Cannot remove coordinator from group " + group);
    Set<Student> members = group.getMembers();
    boolean removed = members.remove(studentService.fetch(studentId));
    if (removed)
      groupRepo.save(group);
    return removed;
  }
}
